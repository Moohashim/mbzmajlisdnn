$(document).ready(function(){
    if($('.lightzoom').length){
        $('.lightzoom').lightzoom({speed: 400, isOverlayClickClosing: true});
    }

    // Partners & Speakers tab

    $('.tab-anchors').on('click', function (){
        $(this).addClass('active-nav').siblings().removeClass('active-nav');
        if($(this).hasClass('partners-nav-tab-item')){
            $('.lobby-partners-section').find('.active-tab').removeClass('active-tab');
        }
        if($(this).hasClass('speakers-nav-tab-item')){
            $('.lobby-speakers-section').find('.active-tab').removeClass('active-tab');
        }

        const activateTab = $(this).attr('data-href');
        $(`#${activateTab}`).addClass('active-tab');
    });

    $('.view-more-btn').on("click", function (event) {
        event.preventDefault();
        const parentBlock = $(this).closest('.active-view-more');

            $(parentBlock[0]).css('opacity', .5)
            setTimeout(() => {
                $(parentBlock[0]).css('opacity', 1)
                $(parentBlock[0]).removeClass('show-less-items');
                $(parentBlock[0]).removeClass('active-view-more');
            }, 500)
    });

    initShowLessItemsBlock();

    function initShowLessItemsBlock(){
        const blockList = $('.show-less-items');


        blockList.each(i => {
            const childrenElementsLength = $(blockList[i]).children().length - 1;
            const photoGallery = $(blockList[i]).closest('.four-items');
            const amountDisplayItems = photoGallery.length ? 12 : 6;

            if(childrenElementsLength > amountDisplayItems && blockList){
                $(blockList[i]).addClass('active-view-more');
            }
        })
    }

    // Right side modal

    $('#open-agenda-modal').on("click", function (event) {
        event.preventDefault();
        openModal('.live-agenda-modal', 'hide-modal');

    });

    $('#close-modal').on("click", function (event) {
        event.preventDefault();
        closeModal('.live-agenda-modal', 'hide-modal');
    });

    $('#open-registration, .registration-btn').on("click", function (event) {
        event.preventDefault();
        openModal('.registration-form-modal', 'hide-modal');
        if(!$('.login-form-modal').hasClass('hide-modal')){
            closeModal('.login-form-modal', 'hide-modal');
        }
        if (window.matchMedia("(max-width: 600px)").matches) {
            $('body').addClass('modal-open');
        }
    });

    $('#close-register-modal').on("click", function (event) {
        event.preventDefault();
        closeModal('.registration-form-modal', 'hide-modal');
        if (window.matchMedia("(max-width: 600px)").matches) {
            $('body').removeClass('modal-open');
        }
    });

    $('#open-login').on("click", function (event) {
        event.preventDefault();
        openModal('.login-form-modal', 'hide-modal');
        if(!$('.registration-form-modal').hasClass('hide-modal')){
            closeModal('.registration-form-modal', 'hide-modal');
        }
        if (window.matchMedia("(max-width: 600px)").matches) {
            $('body').addClass('modal-open');
        }
    });

    $('#close-login-modal').on("click", function (event) {
        event.preventDefault();
        closeModal('.login-form-modal', 'hide-modal');
        if (window.matchMedia("(max-width: 600px)").matches) {
            $('body').removeClass('modal-open');
        }
    });

    $('.mobile-menu-btn').on("click", function (event) {
        event.preventDefault();
        $('.sidebar').toggleClass('show-mobile-nav');
        $(this).toggleClass('changed-mobile-menu-btn');
        $('html').toggleClass('modal-open');
    });

    $('.lobby-nav [href*="#"]').on("click", function (event) {
        event.preventDefault();
        let id  = $(this).attr('href'),
            top = $(id).offset().top ;
        $('body,html').stop().animate({scrollTop: top}, 800);
    });

    function openModal(target, className){
        $(target).toggleClass(className);
    }

    function closeModal(target, className){
        $(target).addClass(className);
    }

    // Top menu

    $( '.lobby' ).hover(function() {
        $('.top-sticky-nav-section').css({
            "opacity": 1,
            "visibility": 'visible'
        });
    });

    $('.close-lobby-menu').on("click", function (event) {
        $('.top-sticky-nav-section').css({
            "opacity": 0,
            "visibility": 'hidden',
            "transition": "opacity .15s linear, visibility .15s .05s"
        });
    });

    // Sidebar links

    const doc = window.document;
    const nav = $('.nav').find('a').not('#not-activate');
    const linksCount = nav.length;
    for (let i = 0; i < linksCount; i++)
        if(doc.URL.endsWith(nav[i].href))
            nav[i].classList.add('active-link')

    // Registration form

    let selectedInstituteArray;

    $('select[name="endrolled-university"]').on('change', function() {
        if(this.value === 'yes') {
            $('#endrolled-university-yes').removeClass('hide-inputs');
        }else{
            $('#endrolled-university-yes').addClass('hide-inputs');
            clearSelectValue('#institution');
            clearSelectValue('#level');
            clearSelectValue('#study-field');
        }
    });

    $('select[name="country"]').on('change', function() {
        if(this.value === 'UAE') {
            $('#selected-country-uae').removeClass('hide-inputs');
            $('#selected-country-outside').addClass('hide-inputs');
            initInstituteSelect();
        }else{
            $('#selected-country-outside').removeClass('hide-inputs');
            $('#selected-country-uae').addClass('hide-inputs');
            clearSelectValue('#institution');
            clearSelectValue('#level');
            clearSelectValue('#study-field');
        }
    });

    if($('select[name="country"]').val()){
        initInstituteSelect();
    }

    $('select[name="institution"]').on('change', function() {
        clearSelectValue('#level');
        clearSelectValue('#study-field');
        initLevelSelect($(this).val());

    });

    $('select[name="level"]').on('change', function() {
        clearSelectValue('#study-field');
        initStudyFieldSelect($(this).val());
    });

    function initInstituteSelect(){
        const select = $('#institution');
        institution.forEach((el, i) => {
            select.innerHTML = select.append(`<option>${el.institute}</option>`);
        })
    }

    function initLevelSelect(institutionName){
        const select = $('#level');
        selectedInstituteArray = institution.filter(item => item.institute == institutionName);
        const selectedInstitute = selectedInstituteArray[0].levels;
        selectedInstitute.forEach((el, i) => {
            select.innerHTML = select.append(`<option>${el.level}</option>`);
        })
    }

    function initStudyFieldSelect(levelName){
        const select = $('#study-field');
        const selectedInstitute = selectedInstituteArray[0].levels;
        const selectedLevelOfSelectedInstituteArray = selectedInstitute.filter(item => item.level == levelName);
        const selectedLevelOfSelectedInstitute = selectedLevelOfSelectedInstituteArray[0].fields;
        selectedLevelOfSelectedInstitute.forEach((el, i) => {
            select.innerHTML = select.append(`<option>${el}</option>`);
        })
    }

    function clearSelectValue(id){
        $(id).children().not(':eq(0)').remove();
        const defaultValue = $(id).children(':eq(0)').val();
        $(id).val(defaultValue);
    }

    //  Modal window

    $('#open-modal').on('click', function (){
        $('.modal-backdrop').addClass('show');
        $('.modal-window').fadeIn(250);
        $('.modal-window').addClass('show');
        $('body').addClass('modal-open');
    });

    $('.btn-close, .bottom-btn-close').on('click', function (){
        $('.modal-backdrop').removeClass('show');
        $('.modal-window').fadeOut(300);
        $('.modal-window').removeClass('show');
        $('body').removeClass('modal-open');
    });

    $(function($){
        $(document).mousedown(function (e){
            let div = $(".modal-dialog");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.modal-backdrop').removeClass('show');
                $('.modal-window').fadeOut(300);
                $('.modal-window').removeClass('show');
                $('body').removeClass('modal-open');
            }
        });
    });


    $( function() {
        if($( "#datepicker" ).length){
            if(document.documentElement.lang == 'en-US'){
                $( "#datepicker" ).datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeYear: true,
                    yearRange: "-100:+0",
                    maxDate: new Date
                });
                $( "#datepicker" ).datepicker( "option", $.datepicker.regional['en'] );
            }else{
                $.datepicker.regional['ar'] = {
                    changeYear: true,
                    yearRange: "-100:+0",
                    maxDate: new Date,
                    closeText: "إغلاق",
                    prevText: "&#x3C;السابق",
                    nextText:"التالي&#x3E;" ,
                    currentText: "اليوم",
                    monthNames: ["كانون الثاني","شباط","آذار","نيسان","مايو","حزيران","تموز","آب","أيلول","تشرين الأول","تشرين الثاني","كانون الأول"],
                    monthNamesShort: ["1","2","3","4","5","6","7","8","9","10","11","12"],
                    dayNames: ["الأحد","الاثنين","الثلاثاء","الأربعاء","الخميس","الجمعة","السبت"],
                    dayNamesShort: ["الأحد","الاثنين","الثلاثاء","الأربعاء","الخميس","الجمعة","السبت"],
                    dayNamesMin: ["ح","ن","ث","ر","خ","ج","س"] ,
                    weekHeader: "أسبوع",
                    dateFormat: "dd/mm/yy",
                    firstDay: 6,
                    isRTL: true,
                    showMonthAfterYear: false,
                    yearSuffix: ""
                };
                $.datepicker.setDefaults($.datepicker.regional['ar']);
                $('#datepicker').datepicker();
            }
        }
    });

    $( function() {
        const listsArray = $('.live-agenda-modal-body').find('ul');
        const descriptionContentArray = $(listsArray[1]).children();
        const descriptionContentAssign = $(listsArray[0]).children();

        descriptionContentArray.each( i => {
           const contentHtml =  $(descriptionContentArray[i]).html();
           $(`<div class="hide-description-block">${contentHtml}</div>`).appendTo(descriptionContentAssign[i])
        });
    });

});


    // Output response message function
    function appendResponse(formType, message) {
        const responseBlock = $(`.${formType}`).find('.form-response-output');
        const color = {
            error: 'red',
            success: 'green'
        };

        responseBlock.html(`<span style="color:${color[message.type]}">${message.message}</span>`);
    }