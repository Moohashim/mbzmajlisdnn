<%@ Control language="cs" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>

<!--#include file="Common/Common.ascx"-->

<div class="container profile-content">

    <!-- USER PROFILE LINK-->
<% if (isLoggedIn){ %>
    <div class="user-bar-my-account">
        <dnn:LOGIN ID="dnnLogin" CssClass="LoginLink" runat="server" LegacyMode="false" />
    </div>
<% } %>
<!--#include file="Common/Menu.ascx"-->
    <main class="maincontent">
        <h3><%= GetString("Profile") %></h3>
	    <div id="ContentPane" runat="server"></div>
    </main>
</div>
