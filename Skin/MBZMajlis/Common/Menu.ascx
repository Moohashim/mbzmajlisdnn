<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="mbz" TagName="Login" Src="~/DesktopModules/MBZMajlis/Views/Login/Login.ascx" %>
<%@ Register TagPrefix="mbz" TagName="Registration" Src="~/DesktopModules/MBZMajlis/Views/Registration/Registration.ascx" %>

<% if (!isLoggedIn){ %>
    <div class="login-form-modal hide-modal">
        <div class="live-agenda-modal-header" id="close-login-modal">
            <img src="<%=SkinPath%>assets/arrow.svg">
        </div>
        <div class="live-agenda-modal-body">
            <div class="login-form-content">
                <h3><%= GetString("Login") %></h3>
                <div class="registration-form-container login-form">
                	<div class="registration-form">
                		<mbz:Login runat="server" ID="Login" />
                	</div>

                </div>
            </div>
        </div>
    </div>
	 <div class="registration-form-modal hide-modal">
                <div class="live-agenda-modal-header" id="close-register-modal">
                    <img src="<%=SkinPath%>assets/arrow.svg">
                </div>
                <div class="live-agenda-modal-body" id="registration-form-modal-body">
                    <div class="thankyouPart"><%= GetString("ThankYou") %></div>
                    <div class="registration-form-content">

                       <div><%= GetString("Registration.Closed") %></div>

                     <%--   <h3><%= GetString("Registration") %></h3>
                        <div class="registration-form-container registration-form">
		                	<div class="registration-form">
		                		<mbz:Registration runat="server" ID="Registration" />
		                	</div>
                        </div>--%>
                    </div>
                </div>
            </div>
<% } %>

<sidebar class="sidebar">
    <ul class="nav">
        <li class="nav-item lobby"><a class="nav-link" href="/lobby"><span><%= GetString("Lobby") %></span></a></li>
		<li class="nav-item majlis">
            <span class="sidebar-coming-soon-text"><%= GetString("ComingSoon") %></span>
            <a class="nav-link" href="javascript:void(0)"><%= GetString("TheMajlis") %></a></li>
		<li class="nav-item hub">
            <span class="sidebar-coming-soon-text"><%= GetString("ComingSoon") %></span>
            <a class="nav-link" href="javascript:void(0)"><%= GetString("TheHub") %></a></li>
        <li class="nav-item orange-bg">
            <div class="block-logo">
                <a class="nav-link" href="/lobby" id="not-activate">
                    <div class="logo-img-block">
                        <img class="logo" alt="" src="<%=SkinPath%>assets/light-logo.svg" />
                        <img class="ar-logo" alt="" src="<%=SkinPath%>assets/logo-ar.svg" />
                    </div>
                    <h4 class="block-logo-name"><%= GetString("MohamedBinZayed") %></h4>
                    <h5 class="block-logo-description"><%= GetString("MajlisforFutureGenerations") %></h5>
                </a>
                <div class="social">
                    <ul>
                        <li>
                            <a href="https://www.instagram.com/mbzfuturegen/" target="_blank">
                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21 7.50002C21 6.6716 21.6716 6.00002 22.5 6.00002C23.3284 6.00002 24 6.6716 24 7.50002C24 8.32845 23.3284 9.00002 22.5 9.00002C21.6716 9.00002 21 8.32845 21 7.50002Z" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M15 7.87502C11.065 7.87502 7.875 11.065 7.875 15C7.875 18.9351 11.065 22.125 15 22.125C18.935 22.125 22.125 18.9351 22.125 15C22.125 11.065 18.935 7.87502 15 7.87502ZM10.125 15C10.125 12.3076 12.3076 10.125 15 10.125C17.6924 10.125 19.875 12.3076 19.875 15C19.875 17.6924 17.6924 19.875 15 19.875C12.3076 19.875 10.125 17.6924 10.125 15Z" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M22.8874 1.24955C17.6875 0.668385 12.3125 0.668385 7.11265 1.24955C4.09457 1.58686 1.65807 3.96438 1.30324 6.99817C0.681432 12.3146 0.681432 17.6854 1.30324 23.0019C1.65807 26.0357 4.09457 28.4132 7.11265 28.7505C12.3125 29.3317 17.6875 29.3317 22.8874 28.7505C25.9055 28.4132 28.342 26.0357 28.6968 23.0019C29.3186 17.6854 29.3186 12.3146 28.6968 6.99817C28.342 3.96438 25.9055 1.58686 22.8874 1.24955ZM7.36257 3.48562C12.3963 2.92303 17.6037 2.92303 22.6375 3.48562C24.6326 3.70861 26.2309 5.28303 26.462 7.25955C27.0635 12.4023 27.0635 17.5977 26.462 22.7405C26.2309 24.717 24.6326 26.2914 22.6375 26.5144C17.6037 27.077 12.3963 27.077 7.36257 26.5144C5.36741 26.2914 3.76918 24.717 3.53801 22.7405C2.93651 17.5977 2.93651 12.4023 3.53801 7.25955C3.76918 5.28303 5.36741 3.70861 7.36257 3.48562Z" fill="white"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/mbzfuturegen" target="_blank">
                                <svg width="34" height="28" viewBox="0 0 34 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M21.5322 1.01524C22.2947 0.884616 23.1487 0.841002 23.9524 0.976965C25.3876 1.21979 26.7134 1.88688 27.7614 2.88702C28.4465 2.90251 29.1197 2.76756 29.7154 2.57516C30.2195 2.41234 30.6454 2.21573 30.9438 2.06027C31.0922 1.98298 31.2068 1.91704 31.2814 1.8724C31.3186 1.85011 31.3456 1.83325 31.3617 1.82306L31.376 1.81397C31.8212 1.51738 32.414 1.5751 32.7933 1.95236C33.1727 2.32971 33.2338 2.92209 32.9394 3.36894C32.6279 3.84188 32.1954 4.68499 31.7108 5.62963C31.6346 5.77816 31.5571 5.9292 31.4786 6.0817C31.2006 6.62164 30.9157 7.16737 30.65 7.63557C30.4832 7.92954 30.3049 8.22801 30.125 8.48592V8.89659C30.1364 10.405 29.9634 11.9056 29.6123 13.3654C29.3914 14.2839 29.0999 15.1863 28.7396 16.0644C27.806 18.3394 26.4284 20.4056 24.6873 22.1422C22.9462 23.8788 20.8765 25.2511 18.5991 26.1789C16.3237 27.1059 13.8866 27.5706 11.4299 27.5459C7.86839 27.549 4.38184 26.5229 1.38976 24.591C0.949059 24.3065 0.763511 23.7551 0.942515 23.262C1.12152 22.7689 1.61756 22.465 2.13817 22.5294C2.58871 22.5852 3.04232 22.6124 3.4963 22.6109C5.12748 22.6064 6.72759 22.2459 8.18838 21.567C7.72405 21.3556 7.28075 21.095 6.86652 20.7878C5.64894 19.885 4.74308 18.625 4.27498 17.1833C4.15334 16.8087 4.2365 16.3977 4.49422 16.0998C4.49883 16.0945 4.5035 16.0892 4.5082 16.084C4.23291 15.8385 3.97531 15.5713 3.73817 15.2838C2.67119 13.9905 2.08368 12.3683 2.07501 10.6917L2.07498 10.6859L2.07499 10.6109C2.07499 10.2121 2.28612 9.84314 2.62992 9.64106C2.71708 9.58983 2.80945 9.55127 2.90448 9.52541C2.39698 8.51279 2.13306 7.39462 2.13499 6.26023C2.13439 4.97508 2.47009 3.71213 3.10873 2.59687C3.29309 2.27492 3.62434 2.06456 3.99413 2.0346C4.36392 2.00464 4.72471 2.15892 4.95851 2.44699C6.40234 4.22601 8.20427 5.68139 10.2473 6.71859C11.2975 7.25177 12.3993 7.66832 13.533 7.96297C14.1686 8.12817 14.8142 8.25504 15.4664 8.34268C15.4376 7.2578 15.6519 6.17349 16.1015 5.1716C16.761 3.70186 17.8897 2.49259 19.3105 1.73346C19.9123 1.41196 20.711 1.1559 21.5322 1.01524ZM4.6149 12.3365C4.80937 12.8838 5.09867 13.3973 5.47376 13.8519C6.21074 14.7453 7.23366 15.3566 8.36949 15.5825C8.87542 15.6832 9.24807 16.115 9.27361 16.6302C9.29915 17.1454 8.97102 17.612 8.47753 17.7622C8.06878 17.8866 7.65002 17.9733 7.22649 18.0214C7.50636 18.3822 7.83558 18.7053 8.20669 18.9805C9.04847 19.6047 10.0639 19.951 11.1116 19.9711C11.587 19.9803 12.0053 20.2873 12.1566 20.7381C12.3078 21.1888 12.1593 21.686 11.7857 21.98C10.2119 23.2183 8.39211 24.0818 6.46378 24.5227C8.06201 25.0345 9.73818 25.298 11.4336 25.2959L11.4467 25.2959C13.6068 25.3183 15.7497 24.9102 17.7502 24.0952C19.7508 23.2802 21.5689 22.0747 23.0983 20.5492C24.6278 19.0236 25.8379 17.2086 26.6581 15.2102C26.9746 14.4388 27.2306 13.6461 27.4247 12.8393C27.7334 11.5556 27.8854 10.2361 27.875 8.90972L27.875 8.90092V8.10593C27.875 7.83315 27.9741 7.56966 28.1539 7.3645C28.2578 7.24594 28.4395 6.9721 28.6931 6.52525C28.934 6.10056 29.2007 5.5907 29.4781 5.05193C29.4948 5.01934 29.5117 4.98659 29.5286 4.95369C28.8092 5.1099 27.9938 5.19179 27.1391 5.09941C26.8684 5.07016 26.6175 4.94373 26.4329 4.74357C25.6786 3.92556 24.6742 3.38105 23.577 3.19544C23.0902 3.11308 22.5017 3.13193 21.9121 3.23294C21.2714 3.34268 20.7138 3.5347 20.3708 3.71796C19.3894 4.24232 18.6098 5.07758 18.1542 6.09276C17.6987 7.10794 17.593 8.24561 17.8537 9.32735C17.9368 9.67221 17.8524 10.0361 17.6259 10.3091C17.3994 10.5822 17.0574 10.7324 16.7032 10.7145C15.438 10.6505 14.1863 10.4575 12.967 10.1406C11.6772 9.80539 10.4236 9.33146 9.22869 8.72484C7.46952 7.83173 5.86744 6.66609 4.48019 5.27455C4.4169 5.59797 4.38477 5.92801 4.38499 6.26018L4.38499 6.26306C4.38342 7.08914 4.58614 7.90278 4.97511 8.63156C5.36407 9.36033 5.92721 9.98162 6.61438 10.4401C7.03141 10.7183 7.214 11.239 7.06212 11.7167C6.91024 12.1945 6.46054 12.5141 5.95939 12.5005C5.5055 12.4882 5.05541 12.433 4.6149 12.3365Z" fill="white"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCcdEf6MRda8B7fRKoVegHcA" target="_blank">
                                <svg width="32" height="24" viewBox="0 0 32 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M13.5788 6.53534C13.2313 6.32681 12.7984 6.32135 12.4457 6.52104C12.093 6.72073 11.875 7.09471 11.875 7.50002V16.5C11.875 16.9053 12.093 17.2793 12.4457 17.479C12.7984 17.6787 13.2313 17.6732 13.5788 17.4647L21.0788 12.9647C21.4177 12.7614 21.625 12.3952 21.625 12C21.625 11.6048 21.4177 11.2386 21.0788 11.0353L13.5788 6.53534ZM18.3134 12L14.125 14.5131V9.48698L18.3134 12Z" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M23.546 0.961936C18.523 0.568962 13.4771 0.568962 8.45408 0.961936L5.09302 1.22489C3.04917 1.38479 1.37715 2.9166 1.03929 4.93866C0.258144 9.61382 0.258144 14.3862 1.03929 19.0614C1.37715 21.0834 3.04917 22.6152 5.09303 22.7751L8.45408 23.0381C13.477 23.4311 18.523 23.4311 23.546 23.0381L26.9071 22.7751C28.9509 22.6152 30.6229 21.0834 30.9608 19.0614C31.7419 14.3862 31.7419 9.61382 30.9608 4.93866C30.6229 2.9166 28.9509 1.38479 26.9071 1.22489L23.546 0.961936ZM8.62957 3.20508C13.5357 2.82125 18.4644 2.82125 23.3705 3.20508L26.7316 3.46803C27.745 3.54732 28.574 4.30685 28.7416 5.30946C29.4817 9.73912 29.4817 14.2609 28.7416 18.6906C28.574 19.6932 27.745 20.4527 26.7316 20.532L23.3705 20.7949C18.4644 21.1788 13.5357 21.1788 8.62957 20.7949L5.26852 20.532C4.2551 20.4527 3.42605 19.6932 3.25853 18.6906C2.5184 14.2609 2.5184 9.73912 3.25853 5.30946C3.42605 4.30685 4.2551 3.54732 5.26852 3.46803L8.62957 3.20508Z" fill="white"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/MBZFutureGen" target="_blank">
                               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 267 266.9" style="enable-background:new 0 0 267 266.9;" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:3.4122;stroke-miterlimit:10;}
                                </style>
                                <path class="st0" d="M194.2,14H72.8c-32.5,0-58.7,26.4-58.7,58.9V194c0,32.5,26.4,58.9,58.7,58.9h121.4c32.5,0,58.7-26.4,58.7-58.9
                                    V72.9C252.9,40.3,226.7,14,194.2,14z M234.5,194c0,22.3-18.1,40.4-40.3,40.4H162v-69h31.4l4.7-36.5H162v-23.3
                                    C162,95.1,165,88,180.1,88h19.3V55.2c-3.4-0.4-14.8-1.4-28.1-1.4c-27.8,0-46.9,16.9-46.9,48.3v27H93.1v36.5h31.5v69H72.8
                                    c-22.2,0-40.3-18.1-40.3-40.4V72.9c0-22.3,18.1-40.4,40.3-40.4h121.4c22.2,0,40.3,18.1,40.3,40.4V194z"/>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="bottom-nav-block">
<% if (!isLoggedIn){ %>
                <div class="login-btn" id="open-login"><%= GetString("Login") %></div>

                <div class="banner-content-register-btn">
                    <p id="open-registration"><%= GetString("Registerhere") %></p>
                </div>
<% } %>

                <div class="switch-lang-block">
                    <span class="language">
                        <dnn:LANGUAGE runat="server" ID="LANGUAGE1" ShowMenu="False" ShowLinks="True" />
                    </span>
                </div>
            </div>
        </li>
    </ul>
</sidebar>

<% if (isLobby){ %>
<div class="top-sticky-nav-section">
    <ul class="lobby-nav">
        <a href="#about">
            <li class="lobby-nav-item"><%= GetString("About") %></li>
        </a>
        <a href="#mission-and-vision">
            <li class="lobby-nav-item"><%= GetString("MissionVision") %></li>
        </a>
        <a href="#speakers">
            <li class="lobby-nav-item"><%= GetString("Speakers") %></li>
        </a>
        <a href="#venues">
            <li class="lobby-nav-item"><%= GetString("Venues") %></li>
        </a>
        <a href="#partners">
            <li class="lobby-nav-item"><%= GetString("Partners") %></li>
        </a>
        <a href="#jubilee-lab">
            <li class="lobby-nav-item"><%= GetString("JubileeLab") %></li>
        </a>
        <a href="#contact-us">
            <li class="lobby-nav-item"><%= GetString("Contactus") %></li>
        </a>
    </ul>
    <span class="close-lobby-menu">&#10006;</span>
</div>
<% } %>

<div class="mobile-menu-btn">
    <div></div>
    <div></div>
    <div></div>
</div>
