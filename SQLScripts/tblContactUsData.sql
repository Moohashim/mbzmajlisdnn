use [MBZMajlis]
GO
IF OBJECT_ID('dbo.tblContactUsData', 'U') IS NOT NULL
 begin
   DROP TABLE [dbo].[tblContactUsData];
 end    
go
  
CREATE TABLE [dbo].[tblContactUsData](
        [PKey]                 [int] IDENTITY(1,1) NOT NULL,
        [FullName]             [nvarchar](250) not null,
        [Email]                [nvarchar](50) not null,
        [Emirate]              [nvarchar](250) null,
        [PhoneNumber]          [nvarchar](25) null,
        [AddInfo]              [ntext] null,
        [CreatedDate]          [datetime] NOT NULL DEFAULT getDATE(),
	
PRIMARY KEY CLUSTERED 
(
 [PKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) 
GO
print 'Create table tblContactUsData Done'
go

GO
