﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MBZMajlis.dbContext
{
    public class entRegistrationData
    {
       public int PKey { get; set; }
       public int? UserId { get; set; }

       public string FirstName { get; set; }
       public string MiddleName { get; set; }
       public string FamilyName { get; set; }
       public string Gender { get; set; }
       public string Email { get; set; }
       public string Emirate { get; set; }
       public string EndrolledUniversity { get; set; }
       public string Country { get; set; }
       public string Institution { get; set; }
       public string Level { get; set; }
       public string StudyField { get; set; }
       public DateTime? Birthday { get; set; }

        public DateTime CreatedDate { get; set; }
       public DateTime? UpdatedDate { get; set; }
    }
}