﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MBZMajlis.dbContext
{
    public class entContactUsData
    {
       public int PKey { get; set; }

       public string FullName { get; set; }
       public string Email { get; set; }
       public string Emirate { get; set; }
       public string PhoneNumber { get; set; }
       public string AddInfo { get; set; }

       public DateTime CreatedDate { get; set; }
    }
}