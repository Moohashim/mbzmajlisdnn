﻿using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;
using MBZMajlis.Controllers.dto;
using MBZMajlis.dbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace MBZMajlis.Service
{
    public class UserService
    {

        public UserInfo CreateUser(dtoUser userObject)
        {
            UserInfo ui;

            var oNewMembership = new UserMembership
            {
                Approved = false,
                CreatedDate = DateTime.UtcNow,
                Password = UserController.GeneratePassword(),
            };

            ui = new UserInfo
            {
                Username = userObject.UserName,
                DisplayName = userObject.DisplayName,
                LastName = userObject.LastName,
                FirstName = userObject.FirstName,
                Email = userObject.Email,
                PortalID = PortalSettings.Current.PortalId,
                Membership = oNewMembership,
                Profile = { PreferredLocale = PortalSettings.Current.DefaultLanguage },
                IsDeleted = false,
            };

            var status = UserController.CreateUser(ref ui);
            
            return ui;

        }

        public bool addRoleToUser(UserInfo user, string roleName)

        {

            bool rc = false;
            RoleController oDnnRoleController = new RoleController();

            RoleInfo newRole = oDnnRoleController.GetRoleByName(user.PortalID, roleName);

            if (newRole != null && user != null)

            {

                rc = user.IsInRole(roleName);

                oDnnRoleController.AddUserRole(user.PortalID, user.UserID, newRole.RoleID, DateTime.MinValue);


                user = UserController.GetUserById(user.PortalID, user.UserID);

                rc = user.IsInRole(roleName);

            }

            return rc;
        }

        public UserInfo UpdateUserDataPwd(entRegistrationData data, string password)
        {
            var portal = PortalSettings.Current.PortalId;
            var ui = UserController.GetUserById(portal, data.UserId.Value);

            if (!string.IsNullOrEmpty(password))
            {
                ui.Membership.UpdatePassword = true;
                ui.Membership.Password = password;
            }

            ui.DisplayName = string.Format("{0} {1} {2}", data.FirstName, data.MiddleName, data.FamilyName);
            ui.LastName = data.FamilyName;
            ui.FirstName = data.FirstName;
            UserController.UpdateUser(portal, ui);

            return ui;

        }

        public bool AuthorizeUser(int userId)
        {
            var user = DotNetNuke.Entities.Users.UserController.GetUserById(PortalSettings.Current.PortalId, userId);

            UserInfo currentUser = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();
            if (!currentUser.IsSuperUser)
                return false;

            if (user != null && !user.IsSuperUser && !user.Membership.Approved && user.IsInRole(Constants.Constants.majilisParticipant))
            {

                entRegistrationData regUser = new RegisterDataService().GetByUserId(user.UserID);
                if (regUser == null)
                {
                    user.Membership.IsDeleted = true;
                    UserController.UpdateUser(PortalSettings.Current.PortalId, user);
                }
                else
                {
                    user.Membership.Approved = true;
                    UserController.UpdateUser(PortalSettings.Current.PortalId, user);
                    UserController.ApproveUser(user);
                }

                return true;
            }

            return false;
        }

        public void AuthorizeUsers(int[] users)
        {
            foreach (var userId in users) {
               AuthorizeUser(userId);
            }
        }


    }
}