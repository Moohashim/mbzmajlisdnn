﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="MBZMajlis.Settings" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<table cellspacing="0" cellpadding="2" border="0" summary="ModuleName1 Settings Design Table">
    <tr>
        <td class="SubHead" width="250" valign="top">
            <asp:label id="lblControlKey" runat="server" controlname="txtControlKey" suffix=":" Text="Key of control to display"></asp:label></td>
        <td valign="bottom" >
            <asp:textbox id="txtControlKey" cssclass="NormalTextBox" width="239px" 
                columns="30" rows="1" maxlength="200" runat="server" />
        </td>
    </tr>
</table>