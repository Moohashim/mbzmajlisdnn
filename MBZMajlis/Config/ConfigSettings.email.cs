﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MBZMajlis.Config
{
    public static partial class  ConfigSettings
    {
        public static string smtpHost {
        get {
               return ConfigurationManager.AppSettings["smtpHost"];
            }
        }

        public static string smtpUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpUserName"];
            }
        }

        public static string smtpPassw
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpPassw"];
            }
        }

        public static int smtpNoSSLPort
        {
            get
            {
                int _smtpNoSSLPort;
                if (!int.TryParse(ConfigurationManager.AppSettings["smtpNoSSLPort"], out _smtpNoSSLPort))
                {
                    throw new InvalidOperationException("Invalid smtpNoSSLPort in web.config");
                }
                return _smtpNoSSLPort;
            }
        }

        public static bool smtpEnableSsl
        {
            get
            {
               bool _smtpEnableSsl;
                if (!bool.TryParse(ConfigurationManager.AppSettings["smtpEnableSsl"], out _smtpEnableSsl))
                {
                    throw new InvalidOperationException("Invalid smtpEnableSsl in web.config");
                }
                return _smtpEnableSsl;
            }
        }

        public static int smtpSSLPort
        {
            get
            {
                int _smtpSSLPort;
                if (!int.TryParse(ConfigurationManager.AppSettings["smtpSSLPort"], out _smtpSSLPort))
                {
                    throw new InvalidOperationException("Invalid smtpSSLPort in web.config");
                }
                return _smtpSSLPort;
            }
        }

        public static string emailFrom
        {
            get
            {
                return ConfigurationManager.AppSettings["emailFrom"];
            }
        }

        public static string emailFromName
        {
            get
            {
                return ConfigurationManager.AppSettings["emailFromName"];
            }
         }

        public static string emailTo
        {
            get
            {
                return ConfigurationManager.AppSettings["emailTo"];
            }
        }


        //public static int digestLastUserId
        //{
        //    get
        //    {
        //        int _digestLastUserId;
        //        if (!int.TryParse(ConfigurationManager.AppSettings["digestLastUserId"], out _digestLastUserId))
        //        {
        //            throw new InvalidOperationException("Invalid digestLastUserId in web.config");
        //        }
        //        return _digestLastUserId;
        //    }

        //    set
        //    {
        //        AddOrUpdateAppSettings("digestLastUserId", Convert.ToString(value));
        //    }
        //}

        public static int watchUserCountForDigest
        {
            get
            {
                int _watchUserCountForDigest;
                if (!int.TryParse(ConfigurationManager.AppSettings["watchUserCountForDigest"], out _watchUserCountForDigest))
                {
                    throw new InvalidOperationException("Invalid watchUserCountForDigest in web.config");
                }
                return _watchUserCountForDigest;
            }
         
        }
     

        public static void AddOrUpdateAppSettings(string key, string value)
        {
            try
            {
                System.Configuration.ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = $"{System.AppDomain.CurrentDomain.BaseDirectory}Web.Config" };
                var configFile = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                              
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                throw new Exception("Error writing app settings");
            }
        }

    }
}