﻿using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Framework;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.UserRequest;
using DotNetNuke.Web.Api;
using MBZMajlis.Controllers.dto;
using MBZMajlis.dbContext;
using MBZMajlis.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Http;

namespace MBZMajlis.Controllers
{
    [DnnAuthorize]
    [AllowAnonymous]
    public class ProfileController : ApiController
    {

        private DNNContext dbContext = new DNNContext();

        [HttpGet]
        public dtoRegistrationData GetProfile(int id)
        {
            var ent = dbContext.RegistrationData.SingleOrDefault(x => x.UserId == id);

            var portal = PortalSettings.Current.PortalId;
            var ui = UserController.GetUserById(portal, id);

            if (ui.IsSuperUser)
                return null;

            if (ent == null || ui == null)
            {
                throw new Exception("User not exist");
            }

            dtoRegistrationData data = new dtoRegistrationData
            {
                Email = ent.Email,
                Emirate = ent.Emirate,
                EndrolledUniversity = ent.EndrolledUniversity,
                Country = ent.Country,
                Institution = ent.Institution,
                Level = ent.Level,
                StudyField = ent.StudyField,
                FamilyName = ent.FamilyName,
                FirstName = ent.FirstName,
                Gender = ent.Gender,
                MiddleName = ent.MiddleName,
                Birthday = ent.Birthday.HasValue ? ent.Birthday.Value.ToString("dd/MM/yyyy") : string.Empty,
            };

            return data;

        }

        [HttpPost]
        public string Profile(dtoRegistrationData data)
        {
           try { 

           var ent = dbContext.RegistrationData.SingleOrDefault(x => x.Email == data.Email);

           var portal = PortalSettings.Current.PortalId;
           var ui = UserController.GetUserById(portal, ent.UserId.Value);

            
            if (ent == null || !ent.UserId.HasValue || ui == null)
            {
                throw new Exception("User with this email does not exist");
            }

            DateTime birthday = DateTime.Now;
            bool convert = false;
            if (!string.IsNullOrEmpty(data.Birthday))
                convert = DateTime.TryParseExact(data.Birthday, new string[]
                   { "d/MM/yyyy", "dd/M/yyyy", "d/M/yyyy", "dd/MM/yyyy", "dd.MM.yyyy", "d.M.yyyy", "d.MM.yyyy", "dd.M.yyyy",
                     "M/dd/yyyy", "M/d/yyyy", "MM/d/yyyy", "MM/dd/yyyy",  "M.d.yyyy", "MM.d.yyyy", "M.dd.yyyy", "MM.dd.yyyy" },
                    System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out birthday);

            ent.Email = data.Email;
                ent.Emirate = data.Emirate;
                ent.EndrolledUniversity = data.EndrolledUniversity;
                ent.Country = data.Country;
                ent.Institution = data.Institution;
                ent.Level = data.Level;
                ent.StudyField = data.StudyField;
                ent.FamilyName = data.FamilyName;
                ent.FirstName = data.FirstName;
                ent.Gender = data.Gender;
                ent.MiddleName = data.MiddleName;
                ent.Birthday = convert ? (DateTime?)birthday : default;

                ent.UpdatedDate = DateTime.Now;


                   new RegisterDataService().UpdateData(ent);  
                   new UserService().UpdateUserDataPwd(ent, data.Password);

                }
                catch(Exception ex)
                {
                  throw new Exception(ex.Message);
                }
            

                return "ok";
        }



        


    }
}