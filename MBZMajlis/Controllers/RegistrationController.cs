﻿using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Framework;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.UserRequest;
using DotNetNuke.Web.Api;
using MBZMajlis.Config;
using MBZMajlis.Controllers.dto;
using MBZMajlis.dbContext;
using MBZMajlis.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace MBZMajlis.Controllers
{
    [DnnAuthorize]
    [AllowAnonymous]
    public class RegistrationController : ApiController
    {

        private DNNContext dbContext = new DNNContext();

        [HttpPost]
        public string Register(dtoRegistrationData data)
        {
            return null;

           try
            {
               var exist = dbContext.RegistrationData.Any(x => x.Email == data.Email);

             var portal = PortalSettings.Current.PortalId;
             var ui = UserController.GetUserByEmail(portal, data.Email);

             if (exist && ui != null && !ui.IsDeleted)
             {
                 throw new Exception("A user with the same email already exists");
             }

                DateTime birthday = DateTime.Now;
                bool convert = false;
                if (!string.IsNullOrEmpty(data.Birthday)) 
                    convert = DateTime.TryParseExact(data.Birthday, new string[]
                     { "d/MM/yyyy", "dd/M/yyyy", "d/M/yyyy", "dd/MM/yyyy", "dd.MM.yyyy", "d.M.yyyy", "d.MM.yyyy", "dd.M.yyyy", 
                       "M/dd/yyyy", "M/d/yyyy", "MM/d/yyyy", "MM/dd/yyyy",  "M.d.yyyy", "MM.d.yyyy", "M.dd.yyyy", "MM.dd.yyyy" },
                        System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out birthday);

                var ent = new entRegistrationData()
                {
                    Email = data.Email,
                    Emirate = data.Emirate,
                    EndrolledUniversity = data.EndrolledUniversity,
                    Country = data.Country,
                    Institution = data.Institution,
                    Level = data.Level,
                    StudyField = data.StudyField,
                    FamilyName = data.FamilyName,
                    FirstName = data.FirstName,
                    Gender = data.Gender,
                    MiddleName = data.MiddleName,
                    Birthday = convert ? (DateTime?)birthday : default,

                    CreatedDate = DateTime.Now
                };


                var userObject = new dtoUser()
                {
                    Email = data.Email,
                    FirstName = data.FirstName,
                    LastName = data.FamilyName,
                    DisplayName = string.Format("{0} {1} {2}", data.FirstName, data.MiddleName, data.FamilyName),
                    UserName = data.Email,
                    PortalID = Convert.ToByte(PortalSettings.Current.PortalId),
                    Roles = new List<string>()
                };

                var user = new UserService().CreateUser(userObject);
                Thread.Sleep(2000);
               
                ent.UserId = user.UserID;
                if (user.UserID > 0)
                {
                    new UserService().addRoleToUser(user, Constants.Constants.majilisParticipant);
                    new RegisterDataService().InsertData(ent);

                    sendDigest(user.UserID);

                   EmailService.SendEmailToUserRegistration(data.Email, data.FirstName);
                }

             }
                catch(Exception ex)
                {
                   throw new Exception(ex.Message);
                }
            
                return "ok";
        }


        private void sendDigest(int userId)
        {
            DigestService ds = new DigestService();

            int digestLastUserId = ds.GetUserId();
            int watchUserCountForDigest = ConfigSettings.watchUserCountForDigest;

            if (userId == digestLastUserId + watchUserCountForDigest)
            {
                List<string> emails = dbContext.RegistrationData
                     .Where(x => x.UserId >= digestLastUserId && x.UserId <= userId)
                     .Select(x=>x.Email).ToList();

                ds.UpdateUserId(userId);
                EmailService.SendEmailAdminRegistrationDigest(emails);
            }
        }
       
       
       
    }
}