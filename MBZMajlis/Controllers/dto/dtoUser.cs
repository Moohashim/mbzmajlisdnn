﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MBZMajlis.Controllers.dto
{
    public class dtoUser
    {

        public int UserID { get; set; }

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
                
        public bool ChangePassword { get; set; }
        public string Password { get; set; }

        public List<string> Roles { get; set; }

        public int CurrentUserId { get; set; }
                
        public byte PortalID { get; set; }
    }
        
}