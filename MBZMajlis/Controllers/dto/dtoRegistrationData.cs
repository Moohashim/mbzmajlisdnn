﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MBZMajlis.Controllers.dto
{
    public class dtoRegistrationData
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string FamilyName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Emirate { get; set; }
        public string EndrolledUniversity { get; set; }
        public string Country { get; set; }
        public string Institution { get; set; }
        public string Level { get; set; }
        public string StudyField { get; set; }

        public string Birthday { get; set; }

        public string Password { get; set; }
    }
}