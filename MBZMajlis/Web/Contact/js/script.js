
    function submitContactData() {

        let fullName = $('input[name="fullName"]').val();
        let email = $('input[name="mail"]').val();
        let emirate = $('input[name="emirate"]').val();
        let phone = $('input[name="phone"]').val();
        let additionalInfo = $('textarea[name="additionalInfo"]').val();

        if ((!email || email == '') || (!fullName || fullName == ''))
            return;

        let dto = {
            FullName : fullName,
            Email: email,
            Emirate: emirate,
            PhoneNumber: phone,
            AddInfo: additionalInfo
        };
             
        $('body').addClass('preload');

        $.ajax({
            type: "POST",
            url: "/API/MBZMajlis/Contact/Contact",
            data: JSON.stringify(dto),
            contentType: 'application/json',
            success: function () {
                             

                $('input[type=text], input[type="email"], input[type="tel"], textarea').val('');
                $('select').find('option').removeAttr("selected");

                appendResponse(errorMessages.contact.formType, errorMessages.contact.success);
                $('body').removeClass('preload');

            }, 
            error: function () {

                appendResponse(errorMessages.contact.formType, errorMessages.contact.error);
                $('body').removeClass('preload');
            }
           
        });
    }

