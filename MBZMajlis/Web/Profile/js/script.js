

function getProfileData(id) {

    if (!id)
        return;
   
       $.ajax({
           type: "GET",
           url: "/API/MBZMajlis/Profile/GetProfile",
           data: "id="+id,
           contentType: 'application/json',
           success: function (data) {

               $('input[name="firstName"]').val(data.FirstName);
               $('input[name="middleName"]').val(data.MiddleName);
               $('input[name="familyName"]').val(data.FamilyName);
               $('select[name="gender"]').val(data.Gender);

               $('input[name="mail"]').val(data.Email);
               $('select[name="emirate"]').val(data.Emirate);

               var dateParts = data.Birthday.split("/");
               var dateBirthday = new Date(+dateParts[2], dateParts[0] - 1, +dateParts[1]); 
               $('input[name="birthday"]').val(dateBirthday.toLocaleDateString());

               $('select[name="endrolled-university"]').val(data.EndrolledUniversity);
               setTimeout(() => { $('select[name="endrolled-university"]').trigger("change") }, 100);

               setTimeout(() => { $('select[name="country"]').val(data.Country) }, 100);
               setTimeout(() => { $('select[name="country"]').trigger("change") }, 100);

               if (data.Country === "UAE") {
                   setTimeout(() => { $('select[name="institution"]').val(data.Institution) }, 100);
                   setTimeout(() => { $('select[name="institution"]').trigger("change") }, 100);
                   
                                                      
                   setTimeout(() => { $('select[name="level"]').val(data.Level) }, 100);
                   setTimeout(() => { $('select[name="level"]').trigger("change") }, 100);                                      

                   setTimeout(() => { $('select[name="study-field"]').val(data.StudyField) }, 100);   
               }
               else {
                   $('input[name="institution"]').val(data.Institution);
                   $('input[name="level"]').val(data.Level);
                   $('input[name="study"]').val(data.StudyField);
               }

           },
           error: function (message) {

               

               alert(message);
           }

       });
    }

    function submitProfileData() {
             
        let firstName = $('input[name="firstName"]').val();
        let middleName = $('input[name="middleName"]').val();
        let familyName = $('input[name="familyName"]').val();
        let gender = $('select[name="gender"]').val();

        let email = $('input[name="mail"]').val();
        let emirate = $('select[name="emirate"]').val();

        let endrolledUniversity = $('select[name="endrolled-university"]').val(); 
        let country = $('select[name="country"]').val(); 

        let birthdayDate = $('input[name="birthday"]').datepicker('getDate');

        let birthday = null;
        if (birthdayDate != null) {
            let d = String(birthdayDate.getDate()).padStart(2, '0');
            let m = String(birthdayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            let y = birthdayDate.getFullYear();
            birthday = d + '/' + m + '/' + y;
        }

        let institution = '';
        let level = '';
        let studyField = '';

        let password = $('input[name="password"]').val();

        if (country !== "Outside UAE") {
            institution = $('select[name="institution"]').val();
            level = $('select[name="level"]').val();
            studyField = $('select[name="study-field"]').val();
        }
        else {
            institution = $('input[name="institution"]').val();
            level = $('input[name="level"]').val();
            studyField = $('input[name="study"]').val();
        }

        let dto = {
            FirstName : firstName,
            MiddleName : middleName,
            FamilyName : familyName,
            Gender: gender,
            Email: email,
            Emirate: emirate,
            EndrolledUniversity: endrolledUniversity,
            Country: country,
            Institution: institution,
            Level: level,
            StudyField: studyField,
            Birthday: birthday,
            Password: password
        };

        $('body').addClass('preload');
       
        $.ajax({
            type: "POST",
            url: "/API/MBZMajlis/Profile/Profile",
            data: JSON.stringify(dto),
            contentType: 'application/json',
            success: function () {
                          
                appendResponse(errorMessages.edit.formType, errorMessages.contact.success);

                $('body').removeClass('preload');
            }, 
            error: function (message) {
                             
                appendResponse(errorMessages.edit.formType, errorMessages.contact.error);
                $('body').removeClass('preload');
            }
           
        });
    }
