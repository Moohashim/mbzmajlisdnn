﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="MBZMajlis.Views.Login.Login" %>

<% Func<string, string> GetString = (s) => DotNetNuke.Services.Localization.Localization.GetString(s, "~/App_GlobalResources/MBZMajlis.resx"); %>
<%--<div>
    <p class="email-group">
        <input type="email" name="email" placeholder="Email" required=""  id="email">
    </p>
    <p>
        <input type="password" name="password" placeholder="Password" required=""  id="password">
    </p>

   <input type="submit" value="Log in" id="button-login">

</div>--%>
<%--<div class="form-response-output"></div>--%>
<asp:UpdatePanel ID="updatePnl" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                    <form name="LogInForm">
                    <div>
                        <div runat="server" id="Step1">
                            <p class="email-group">
                                <input type="email" name="email" required="" runat="server" id="email"/>
                            </p>
                            <p class="password-group">
                                <input type="password" name="password" required="" runat="server" id="password"/>
                            </p>
                        </div>
                        <div runat="server" id="Step2" visible="false">
                            A verification code has been sent to your email
                            <p class="password-group">
                                <input type="text" name="code" required="" runat="server" id="code"/>
                            </p>
                        </div>
                        <asp:Button ID="Button2" OnClick="LogIn_Click" runat="server" />
                    </div>

                    <div class="form-response-output">
                        <asp:Label runat="server" ID="lblError" />
                    </div>
  
                     </form>
                </ContentTemplate>
   </asp:UpdatePanel>
                          

<%-- <script src='<%= ResolveUrl("../../Web/Login/js/script.js") %>'></script>--%>
